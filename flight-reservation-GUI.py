#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

# to install Qt for Python:
# sudo apt-get install python3-pyqt4

from PyQt4 import QtGui, QtCore
import sys
import os

from sources.customMainWindow import customMainWindow
from sources.popupChooseCountry import popupChooseCountry

def main():
	app = QtGui.QApplication(sys.argv)

	window = customMainWindow()
	window.show()

	# to test if popup request focus or not
	# country_choices = list()
	# country_choices.append("France")
	# country_choices.append("United States")
	# country_choices.append("United Kingdom")
	# popup = popupChooseCountry(country_choices)
	# popup.show()

	app.exec_()

if  __name__ == '__main__':
	main()