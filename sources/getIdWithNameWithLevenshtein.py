#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    #
    #   Found on www.hetland.org
    #
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]

def score(name, searched_name):
    name_array = name.split() 
    searched_name_array = searched_name.split() 
    searched_name_array = searched_name.split()
    for part in name_array:
       try:
         searched_name_array.remove(part)
         name_array.remove(part)
       except ValueError:
         pass
    score = 0
    n = len(searched_name_array)
    if n > 0 :
       for searched_part in searched_name_array:
           lev = []
           for part in name_array :
               lev.append(levenshtein(searched_part, part))
           score += min(lev)
    return score

def getIdWithNameWithLevenshtein(name, id_list):
	"""Return the list of id having a city with the name given as argument"""

	name = name.upper()
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	conn.create_function("score", 2, score)
	query = "SELECT id FROM airports WHERE score(UPPER(city),'"+name+"')<=1"

	print query
	c.execute(query)


	# we create a list to store matching countries
	for line in c.fetchall():
		id_list.append(line[0])

	c.close()
	return id_list

def test():

	lista = list()
	print getIdWithName("Paris", lista)
	print
	print getIdWithName("Pari", lista)

if __name__ == '__main__':
	test()
