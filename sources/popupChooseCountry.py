#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

from PyQt4 import QtGui, QtCore
import sys
import os

class popupChooseCountry(QtGui.QDialog):
	"""Customized popup to ask for a country"""

	def __init__(self,country_choices,title):
		QtGui.QDialog.__init__(self)

		self.setWindowTitle(title + " ?")

		# layout vertical pour proposer tous les choix
		layout = QtGui.QVBoxLayout()
		self.setLayout(layout)
		self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

		label = QtGui.QLabel("Please select a country", self)
		layout.addWidget(label)

		self.button_list = []
		for n in range(0,len(country_choices)):
			button_name = "button_" + str(n)
			button_name = QtGui.QRadioButton(country_choices[n],self)
			layout.addWidget(button_name)
			self.button_list.append(button_name)

		self.checked = None
		button = QtGui.QPushButton("OK")
		self.connect(button,QtCore.SIGNAL("clicked()"),self.ok)
		layout.addWidget(button)

		self.resize(200, 70+30*len(country_choices))
		self.setFixedSize(self.size())

	def ok(self):
		"""Put the number (from 0) of checked button in self.checked"""
		# for i, b in enumerate(self.button_list):
		# 	if b.isChecked():
		# 		print "radio button num " + str(i) + " is checked"
		# 	else:
		# 		print "radio button num " + str(i) + " is NOT checked"
		for i, b in enumerate(self.button_list):
			if b.isChecked():
				self.checked = i
		self.close()

	def getChecked(self):
		return self.checked




def test():
	app = QtGui.QApplication(sys.argv)

	country_choices = list()
	country_choices.append("France")
	country_choices.append("United States")
	country_choices.append("United Kingdom")

	popup = popupChooseCountry(country_choices)
	popup.show()
	# WARNING: if you want to use a popup in a program where there are multiple windows,
	# when you write popup.show(), the popup does not appears
	# YOU MUST USE popup.exec_() INSTEAD 

	app.exec_()


if  __name__ == '__main__':
	test()
		
