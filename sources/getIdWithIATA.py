#!/usr/bin/python3.2
# -*-coding:Utf-8 -*   # linux

import sqlite3

def getIdWithIATA(iata,id_list):
	"""Return the (distinct) list of countries having a city with the name given as argument"""

	iata = iata.upper()
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	query = "SELECT id FROM airports WHERE UPPER(IATA)='" + iata + "' "
	c.execute(query)

	# we create a list to store matching countries
	for line in c.fetchall():
		id_list.append(line[0])

	c.close()
	return id_list

def test():
	id = list()
	id.append("useless")
	id = getIdWithIATA("CDG",id)  # id should be 1382
	for n in range(0,len(id)):
		print id[n]

if  __name__ == '__main__':
	test()