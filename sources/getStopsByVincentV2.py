#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

# TEST: en console, depuis la racine du projet:
#  python sources/getStopsByVincent.py   

import sqlite3
import sys

def getMinFromTime(time):
	"""convert time format "HH:MM" to minutes"""
	l = time.split(':')   #  => l[0] = "HH"   l[1] = "MM"
	return int(l[0]) * 60 + int(l[1])

def formatTimeFromMin(duration):
	"""INPUT =  duration (in minutes) -> OUTPUT = "HH:MM" """
	minutes = int(duration)%60
	hours = int(duration)/60
	return doubleIntToTimeString(hours, minutes)

def doubleIntToTimeString(hours, minutes):
	if hours < 10:
		hours_string = "0" + str(hours)
	else:
		hours_string = str(hours)
	if minutes <10:
		minutes_string = "0" + str(minutes)
	else:
		minutes_string = str(minutes)
	return hours_string + ":" + minutes_string	


def isItNextDayTime(time):
	"""INPUT: HH:MM. Return 1 if time > 23:59, else return 0"""
	l = time.split(':')	
	if int(l[0]) > 23:
		return 1
	else:
		return 0 

def isItNextDayDuration(duration):
	"""INPUT: duration in minutes. Return 1 if duration > 24*60, else return 0"""
	if duration >= 24*60:
		return 1
	else:
		return 0 

def nextWeekDay(day):
	"""To get the number of the next day"""
	if day < 7:
		day = day + 1
	else:  # day = 7
		day = 1
	return day

def computeLocalArrivalTime(departure_time, duration, timezone_gap):
	"""Output time format: HH:MM"""
	time = departure_time.split(':')
	hours = int(time[0])
	minutes = int(time[1])	
	# we add duration
	hours += int(duration)/60
	minutes += int(duration)%60
	if minutes > 59:
		minutes = minutes % 60
		hours = hours + 1
	hours = hours + timezone_gap
	hours = hours % 24
	time_without_day = doubleIntToTimeString(hours, minutes)
	return time_without_day

def getFlag(departure_time, duration, timezone_gap):
	"""Flag 0 if same day, flag 1 if day + 1"""
	time = departure_time.split(':')
	hours = int(time[0])
	minutes = int(time[1])	
	# we add duration
	hours += int(duration)/60
	minutes += int(duration)%60
	if minutes > 59:
		minutes = minutes % 60
		hours = hours + 1
	hours = hours + timezone_gap
	if hours > 23:
		hours = hours % 24
		return 1
	if hours < 0:  # shouls appen very rarely
		hours = hours % 24
		return -1
	return 0

def addMargin(time, duration):
	"""INPUT: HH:MM + duration (minutes). OUTPUT: HH:MM"""
	time = time.split(':')
	hours = int(time[0])
	minutes = int(time[1])	
	# we add duration
	hours += int(duration)/60
	minutes += int(duration)%60
	if minutes > 59:
		minutes = minutes % 60
		hours = hours + 1
	# IL NE FAUT PAS FAIRE DE MODULO SUR 24 HEURES, C'EST FAIT EXPRES
	time_without_day = doubleIntToTimeString(hours, minutes)
	return time_without_day

def prettifyText(string,i):
	"""To prettify the output text displayed when test in the console"""
	n = i - len(string)
	string = string + " "*n
	return string


def getStopsByVincentV2(departure_list, arrival_list, day, flag_same_company):
	"""To get stops (une escale uniquement)"""

	margin_min_between_flights = 90    # minutes
	margin_max_between_flights = 300   # minutes

	next_day = nextWeekDay(day)

	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()

	# we create SQL functions from Python functions, to deal with time formats
	conn.create_function("getMinFromTime", 1, getMinFromTime)
	conn.create_function("formatTimeFromMin", 1, formatTimeFromMin)
	conn.create_function("computeLocalArrivalTime", 3, computeLocalArrivalTime)
	conn.create_function("getFlag", 3, getFlag)
	conn.create_function("addMargin", 2, addMargin)

	# VERSION 4: on fait les requêtes séparéments, donc une pour J, une pour J+1, ...
	# pour départ après escale le jour même
	query = ""

	flag_union_all = False
	for i in departure_list:
		for j in arrival_list:

			if flag_union_all == True:
				query += """ UNION ALL """
			else:
				flag_union_all = True
				

			query += """SELECT f1.departure, f1.arrival, f1.day_op, f1.dep_time, f1.duration, a2.timezone - a1.timezone AS dec_horaire, 
				computeLocalArrivalTime(f1.dep_time,f1.duration,a2.timezone - a1.timezone) AS stop_local_arrival_time, f1.flightnum,
				getFlag(f1.dep_time,f1.duration,a2.timezone - a1.timezone) AS flag_j_plus_un,
				f2.departure, f2.arrival, f2.day_op, f2.dep_time, f2.duration, a4.timezone - a2.timezone AS dec_horaire,
				computeLocalArrivalTime(f2.dep_time,f2.duration,a4.timezone - a2.timezone) AS final_local_arrival_time, f2.flightnum, 0
			FROM flights AS f1 
			INNER JOIN flights AS f2 ON f1.arrival=f2.departure 
			INNER JOIN airports AS a1 ON a1.IATA = f1.departure
			INNER JOIN airports AS a2 ON a2.IATA = f1.arrival
			INNER JOIN airports AS a4 ON a4.IATA = f2.arrival
			WHERE f1.departure=""" + "'" + str(i) + "'" + """ AND f2.arrival=""" + "'" + str(j) + "'"

			if flag_same_company is True:
				query += """ AND f1.carrier = f2.carrier """
			
			query += """
				AND f1.day_op LIKE '%""" + str(day) + """%' 
				AND f2.day_op LIKE '%""" + str(day) + """%' 
				AND f2.dep_time BETWEEN addMargin(stop_local_arrival_time, 60) AND addMargin(stop_local_arrival_time, 300)
			UNION ALL
				SELECT f1.departure, f1.arrival, f1.day_op, f1.dep_time, f1.duration, a2.timezone - a1.timezone AS dec_horaire, 
				computeLocalArrivalTime(f1.dep_time,f1.duration,a2.timezone - a1.timezone) AS stop_local_arrival_time, f1.flightnum,
				getFlag(f1.dep_time,f1.duration,a2.timezone - a1.timezone) AS flag_j_plus_un,
				f2.departure, f2.arrival, f2.day_op, f2.dep_time, f2.duration, a4.timezone - a2.timezone AS dec_horaire,
				computeLocalArrivalTime(f2.dep_time,f2.duration,a4.timezone - a2.timezone) AS final_local_arrival_time, f2.flightnum, 1
			FROM flights AS f1 
			INNER JOIN flights AS f2 ON f1.arrival=f2.departure 
			INNER JOIN airports AS a1 ON a1.IATA = f1.departure
			INNER JOIN airports AS a2 ON a2.IATA = f1.arrival
			INNER JOIN airports AS a4 ON a4.IATA = f2.arrival
			WHERE f1.departure=""" + "'" + str(i) + "'" + """ AND f2.arrival=""" + "'" + str(j) + "'" 

			if flag_same_company is True:
				query += """ AND f1.carrier = f2.carrier """
				
			query += """
				AND f1.day_op LIKE '%""" + str(day) + """%' 
				AND f2.day_op LIKE '%""" + str(next_day) + """%' 
				AND f2.dep_time BETWEEN '00:00' AND '05:00' """

	query += ";"
	print query

	# IDEE: JOINDRE LES 2 REQUETES AVEC UN UNION ALL (ET DANS LA REQUETE 1)
	# ensuite, on a le flag J+1 pour savoir qui permet de savoir si l'arrivée du premier vol est sur le jour d'après ou pas
	# et on a le flag de la dernière colonne pour savoir si le second vol est le jour J ou J+1


	c.execute(query)
	print "query done"

	#liste = list()
	#for line in c.fetchall():
	#	print ""   # to jump a line
	#	l = list()
	#	for i in range(0,18):
			# if i==7:
			# 	print ""    # to jump a line
			# 	sys.stdout.write("      ")
	#		l.append(str(line[i]))
	#		if i==0 or i==1 or i==9 or i==10:
	#			sys.stdout.write(prettifyText(str(line[i]),4))
	#		elif i==8 or i==17:  # flag
	#			sys.stdout.write(prettifyText(str(line[i]),2))
	#		else:
	#			sys.stdout.write(prettifyText(str(line[i]),8))
			# sys.stdout.write("   ")
	#	liste.append(l)
		# min = getMinFromTime(line[3]) + int(line[4]) + 90
		# max = getMinFromTime(line[3]) + int(line[4]) + 300
		# print " --> " + formatTimeFromMin(min) + "  " + formatTimeFromMin(max)
		# print ""   # to jump a line

	# liste.append(c.fetchone()[0])
	
	
	# liste = list(c)
	# print liste

	l = listFilter(c.fetchall())
	c.close()

	return l

def listFilter(stopsList):

	liste = list()

	for i in range(0,len(stopsList)):
		A = getMinFromTime(stopsList[i][6])
		B = getMinFromTime(stopsList[i][12])
		
		if ((B > A) and ((B-A) >= 60) and ((B-A) <= 300)) or ((B < A) and ((B+1440-A) >= 60) and ((B+1440-A) <= 300)):
			liste.append(stopsList[i])
			print(stopsList[i])

	return liste



def test():
	day = 7
	# print addMargin("14:00", 90)
	dep_list = list()
	dep_list.append("CDG")
	dep_list.append("ORY")

	arr_list = list()
	arr_list.append("JFK")

	print getStopsByVincentV2(dep_list, arr_list, day, True)

	# print computeLocalArrivalTime("00:01", 140, -7)
	

if  __name__ == '__main__':
	test()

