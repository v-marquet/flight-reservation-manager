#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def getCountriesWhereCity(city):
	"""Return the (distinct) list of countries having a city with the name given as argument"""

	city = city.upper()
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	query = "SELECT DISTINCT country FROM airports WHERE UPPER(city)='" + city + "' "
	c.execute(query)

	# we create a list to store matching countries
	countries = list()
	for line in c.fetchall():
		countries.append(line[0])

	c.close()
	return countries

def test():
	countries = getCountriesWhereCity("London")
	for n in range(0,len(countries)):
		print countries[n]

if  __name__ == '__main__':
	test()
