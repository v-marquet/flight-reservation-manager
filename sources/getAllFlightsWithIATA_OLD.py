# première version de getAllFlightsWithIATA, par Michel
# problème: beaucoup trop lent (les requêtes sont effectuées séparément pour chaque conbinairon départ/arrivée)

def directFlightCheckOneAtATime(departure, arrival, date):
	"""get all flights information for a departure, arrival and a date"""
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	conn.create_function("instr", 2, instr)
	query = "SELECT rowid FROM flights WHERE (departure = '"+departure+"') AND (arrival = '"+arrival+"') AND (instr(day_op, '"+date+"') != 0)"
	c.execute(query)

	liste = list(c)

	c.close()

	return liste


def getAllFlightsWithIataByMichel(depList, arrList, dayOfWeek):
	"""get all flights information for a departure List and an arrival List"""
	print "DEBUG: entering getAllFlightsWithIATA ..."
	for departures in depList:
		for arrivals in arrList:
			if len(directFlightCheckOneAtATime(departures, arrivals, str(dayOfWeek))) == 0:
			  print departures + ' ' + arrivals
				print 'No flight available'
			if len(directFlightCheckOneAtATime(departures, arrivals, str(dayOfWeek))) != 0:
				print departures + ' ' + arrivals
				print directFlightCheckOneAtATime(departures, arrivals, str(dayOfWeek))
	print "DEBUG: leaving getAllFlightsWithIATA"
	return

