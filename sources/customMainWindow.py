#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

from PyQt4 import QtGui, QtCore
import sys
import os
from getCountriesWhereID import *
from getIdWithIATA import *
from getIdWithNameWithLevenshtein import *
from getIdWithNameWithoutLevenshtein import *
from popupChooseCountry import *
from getAllFlightsWithIATA import *
from changeListIDtoIATA import *
from convertGeopositionToPixel import *
from getGeolocalizationFromID import *
from filterByCountry import *
from getStopsByVincentV2 import *
from getStopsByVincentV3 import *
from autoComplete import *

class customMainWindow(QtGui.QMainWindow):

	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.DEBUG = True
		if self.DEBUG == True:
			print "\nDEBUG MODE ACTIVATED"
		self.main_widget = None
		self.main_layout = None
		self.QLE_departure = None
		self.QLE_arrival = None
		self.completer_dep = None
		self.completer_arr = None
		self.stringList_dep = None
		self.stringList_arr = None
		self.calendar = None
		self.check_box = None
		self.list_view = None
		self.list_model = None
		self.item_list = list()
		self.auto_complete = True  # to activate auto completion
		self.init()

	def newSearch(self):
		# we convert from QString to usual Python string
		departure = str(self.QLE_departure.text())
		arrival = str(self.QLE_arrival.text())
		day_of_week = str(self.calendar.selectedDate().dayOfWeek())
		stops = self.check_box.isChecked()

		print "\n=== NEW SEARCH ==="
		print "Departure : " + departure
		print "Arrival : " + arrival
		print "Weekday : " + day_of_week
		print "Stops ? " + str(stops) + "\n"

		if len(departure)==0 or len(arrival)==0:
			self.updateList(list())
			return

		# we create lists to keep matching (in some way) departure and arrival airports
		id_departure = list()
		id_arrival = list()

		# if the number of letters is three, this is maybe a IATA code
		# so we search for an aiport with this code
		# we add the id of corresponding airport to the list of interesting airports
		if len(departure) == 3:
			print "Departure : this is maybe a IATA code"
			id_departure = getIdWithIATA(departure, id_departure)
		if len(arrival) == 3:
			print "Arrival : this is maybe a IATA code"
			id_arrival = getIdWithIATA(arrival, id_arrival)

		# we also search for airports where the city match the string entered by the user 
		# we add the id of corresponding airports to the list of interesting airports
		id_departure = getIdWithNameWithoutLevenshtein(departure, id_departure)
		id_arrival = getIdWithNameWithoutLevenshtein(arrival, id_arrival)

		# if there's no results, we try with Levenshtein score = 1
		if len(id_departure)==0:
			if self.DEBUG==True:
				print "DEBUG: Applying Levenshtein score to departure"
			id_departure = getIdWithNameWithLevenshtein(departure, id_departure)
		if len(id_arrival)==0:
			if self.DEBUG==True:
				print "DEBUG: Applying Levenshtein score to arrival"
			id_arrival = getIdWithNameWithLevenshtein(arrival, id_arrival)

		# after IATA and city name searches, a line can appear twice
		id_departure = list(set(id_departure))
		id_arrival = list(set(id_arrival))

		# DEBUG
		if self.DEBUG == True:
			print "DEBUG: before filtering with country:"
			print "  DEBUG: departure: there are " + str(len(id_departure)) + " id in id_departure"
			print "  DEBUG: arrival:   there are " + str(len(id_arrival)) + " id in id_arrival"

		# so now we have a list of the id of interseting airports
		# but due to the fact that sometimes some cities in different countries have the same name
		# so we check if all the airports id refer to the same country
		# if not, we ask the user to choose which country
		# for exemple, for "London", the user must indicate if it's the town in UK, in US or in Canada
		countries_departure = getCountriesWhereID(id_departure)
		countries_arrival = getCountriesWhereID(id_arrival)
		print "There is/are " + str(len(countries_departure)) + " countrie(s) where there is a match:"
		for c in countries_departure:
			print "- " + c
		print "There is/are " + str(len(countries_arrival)) + " countrie(s) where there is a match:"
		for c in countries_arrival:
			print "- " + c

		# we display a popup if there is a doubt about the city
		# (for example, 3 countries have a 'London' town)
		if len(countries_departure) > 1:
			popup1 = popupChooseCountry(countries_departure,"Departure")
			popup1.exec_()
			checked = popup1.getChecked()
			if checked is None:  # if user clicked the "cancel" button
				print "Search canceled"
				return
			print "You choose " + countries_departure[checked]
			# we remove id in a different country
			id_departure = filterByCountry(id_departure,countries_departure[checked])

		if len(countries_arrival) > 1:
			popup2 = popupChooseCountry(countries_arrival,"Arrival")
			popup2.exec_()
			checked = popup2.getChecked()
			if checked is None:  # if user clicked the "cancel" button
				print "Search canceled"
				return
			print "You choose " + countries_arrival[checked]
			# we remove id in a different country
			id_arrival = filterByCountry(id_arrival,countries_arrival[checked])

		# DEBUG
		if self.DEBUG == True:
			print "DEBUG: after filtering with country:"
			print "  DEBUG: departure: there are " + str(len(id_departure)) + " id in id_departure"
			print "  DEBUG: arrival:   there are " + str(len(id_arrival)) + " id in id_arrival"

		# we draw on the map
		self.updateMap(id_departure,id_arrival)

		# we convert the id list to a IATA list
		IATA_departure = changeListIDtoIATA(id_departure)
		IATA_arrival = changeListIDtoIATA(id_arrival)

		# with the IATA lists, we search into the "fligths" database FOR DIRECT FLIGHTS
		final_list_with_flights = getAllFlightsWithIATA(IATA_departure, IATA_arrival, day_of_week)

		if stops == True:
			final_list_with_flights_with_stops = getStopsByVincentV3(IATA_departure, IATA_arrival, day_of_week, self.check_box_company.isChecked())
		else:
			final_list_with_flights_with_stops = list()

		# mise à jour de l'affichage en dessous du tableau de la liste des vols trouvés
		self.updateList(final_list_with_flights, final_list_with_flights_with_stops)

		print "=== Search complete ==="

	def updateAutoCompleteDeparture(self):
		if self.auto_complete == True:
			if self.DEBUG == True:
				print "AUTO COMPLETE"

			dep = str(self.QLE_departure.text())
			dep = dep.lower()
			print dep
			if len(dep) > 0:
				c = dep[0]
				c = c.upper()
				dep2 = c
				dep2 += dep[1:]

			self.QLE_departure.setText(dep2)

			l = autoComplete(dep2)

			self.stringList_dep = QtGui.QStringListModel(l,parent=self)
			self.completer_dep.setModel(self.stringList_dep)

	def updateAutoCompleteArrival(self):
		if self.auto_complete == True:
			if self.DEBUG == True:
				print "AUTO COMPLETE"

			arr = str(self.QLE_arrival.text())
			arr = arr.lower()
			print arr
			if len(arr) > 0:
				c = arr[0]
				c = c.upper()
				arr2 = c
				arr2 += arr[1:]

			self.QLE_arrival.setText(arr2)

			l = autoComplete(str(self.QLE_arrival.text()))

			self.stringList_arr = QtGui.QStringListModel(l,parent=self)
			self.completer_arr.setModel(self.stringList_arr)

			

			# l = QtCore.QStringList()
			# l.append("TEST1")
			# l.append("TEST2")

			# completer = QtGui.QCompleter(l, self.QLE_departure)
			# self.QLE_departure.setCompleter(completer)

				

			# méthode comme le code C++
			# l = QtCore.QStringList()
			# l.append("United States")
			# l.append("United Kingdom")
			# completer = QtGui.QCompleter(l)
			# self.QLE_departure.setCompleter(completer)

			# méthode comme le code python avec /tmp1
			# completer=QtGui.QCompleter(QtCore.QStringList(['/tmp/'+i for i in 'abcdefg']), parent=self)
			# completer.setMaxVisibleItems(5)
			# self.completer=completer

			# self.QLE_departure.setCompleter(completer)

			# self.QLE_departure=self.QLE_departure

	def init(self):
		# we create the main window
		self.setWindowTitle("Flight reservation")
		icon = QtGui.QIcon("images/plane.jpg")
		self.setWindowIcon(icon)  # to set the icon of the program in launch bar
		self.resize(1100, 655)  # set size (before adding QListView, height was 420)
		self.setFixedSize(self.size())  # to prevent from resizing
		self.move(300, 100)  # to set the initial position of the window when launching the program

		self.main_widget = QtGui.QWidget()
		self.setCentralWidget(self.main_widget)

		self.main_layout = QtGui.QHBoxLayout()
		self.main_widget.setLayout(self.main_layout)

		# left zone of the window: we get what the user wants
		edit_widget = QtGui.QWidget(self)
		self.main_layout.addWidget(edit_widget)

		# layout_left = QtGui.QGridLayout()
		begin_x = 10  # margin of the widgets from the left side of the window
		begin_y = 10  # margin of the widgets from the upper side of the window
		margin_y = 30  # vertical margin between 2 widgets 

		label_departure = QtGui.QLabel("Departure city / airport", self.main_widget)
		label_departure.setFixedSize(150,30)
		label_departure.move(begin_x,begin_y)

		self.QLE_departure = QtGui.QLineEdit(self.main_widget)
		self.QLE_departure.setFixedSize(122,30)
		self.QLE_departure.move(begin_x+150,begin_y)

		if self.auto_complete == True:
			self.QLE_departure.textChanged.connect(lambda: self.updateAutoCompleteDeparture())

		label_arrival = QtGui.QLabel("Arrival city / airport", self.main_widget)
		label_arrival.setFixedSize(150,30)
		label_arrival.move(begin_x,begin_y+margin_y)

		self.QLE_arrival = QtGui.QLineEdit(self.main_widget)
		self.QLE_arrival.setFixedSize(122,30)
		self.QLE_arrival.move(begin_x+150,begin_y+margin_y)

		if self.auto_complete == True:
			self.QLE_arrival.textChanged.connect(lambda: self.updateAutoCompleteArrival())

		if self.auto_complete == True:
			self.stringList_dep = QtGui.QStringListModel()
			self.completer_dep = QtGui.QCompleter(self.QLE_departure)
			# self.completer.setMaxVisibleItems(5)
			self.completer_dep.setModel(self.stringList_dep)
			# self.stringList.setParent(self.completer)
			# self.completer=self.completer
			self.QLE_departure.setCompleter(self.completer_dep)

			self.stringList_arr = QtGui.QStringListModel()
			self.completer_arr = QtGui.QCompleter(self.QLE_arrival)
			self.completer_arr.setModel(self.stringList_arr)
			self.QLE_arrival.setCompleter(self.completer_arr)

		# we create a calendar to get the departure date
		self.calendar = QtGui.QCalendarWidget(self.main_widget)
		self.calendar.setGeometry(0, 0, 270, 270)
		self.calendar.move(begin_x,begin_y+margin_y*2+10)
		# layout_left.addWidget(self.calendar)

		# check box for the 'stops' option
		self.check_box = QtGui.QCheckBox("With stops ?", self.main_widget)
		self.check_box.setFixedSize(100,30)
		self.check_box.move(begin_x+15,350)

		# check box for the 'stops' option
		self.check_box_company = QtGui.QCheckBox("Same company ?", self.main_widget)
		self.check_box_company.setFixedSize(140,30)
		self.check_box_company.move(begin_x+125,350)

		# the "Search" button
		search = QtGui.QPushButton("Search !", self.main_widget)
		search.setFixedSize(100,30)
		search.move(begin_x+80,385)
		# layout_left.addWidget(search)

		# gestion de l'affichage de la liste des vols
		self.list_view = QtGui.QTreeView()
		# self.list_model = QtGui.QStandardItemModel(0,4)  # 0 lignes, 4 colonne
		# self.list_model.setHorizontalHeaderItem(0, QtGui.QStandardItem("Departure Airport"))
		# self.list_model.setHorizontalHeaderItem(1, QtGui.QStandardItem("Departure date"))
		# self.list_model.setHorizontalHeaderItem(2, QtGui.QStandardItem("Arrival Airport"))
		# self.list_model.setHorizontalHeaderItem(3, QtGui.QStandardItem("Arrival date"))
		# self.list_view.setModel(self.list_model)

		split = QtGui.QSplitter(self.main_widget)
		split.setFixedSize(1082, 220)
		split.move(10, 425)

		split.addWidget(self.list_view)


		self.connect(search,QtCore.SIGNAL("clicked()"),self.newSearch)

		# right zone of the window: we display the map
		self.map_label = QtGui.QWidget(self.main_widget)
		# self.map_label.setFixedSize(800,403)
		# self.map_label.setGeometry(0,0,853, 480)
		self.main_layout.addWidget(self.map_label)

		layout_test = QtGui.QGridLayout()
		self.map_label.setLayout(layout_test)

		# # wall = QtGui.QLabel(window)
		# # wall.setGeometry(0, 0, 853, 480)
		# # use full ABSOLUTE path to the image, not relative
		# self.map_pix = QtGui.QPixmap(os.getcwd() + "/images/equirectangular_projection_800.jpg")
		# self.pen = QtGui.QPainter(self.map_pix)
		# # to prevent the "QPaintDevice: Cannot destroy paint device that is being painted" error
		# # put this code not in a function but in "directly read code" mode
		# self.pen.setPen(QtGui.QColor(223, 47, 32))
		# self.pen.setBrush(QtGui.QColor(223, 47, 32))
		# self.pen.drawEllipse(404,94,5,5)
		# self.map_label.setPixmap(self.map_pix)

		self.scene = QtGui.QGraphicsScene()
		self.scene.setSceneRect(0, 0, 800, 400)
		# item = QtGui.QGraphicsItem()
		image = QtGui.QPixmap(os.getcwd() + "/images/equirectangular_projection_800.jpg")
		# image = QtGui.QPixmap()
		# image.load(os.getcwd() + "/images/equirectangular_projection_800.jpg");

		item = self.scene.addPixmap(image)
		# item.setPos(300,300)

		# for an unknown reason, when adding the QGraphicsView to a layout, it does not respect the layout
		# so I use absolute positioning with setGeometry instead
		vue = QtGui.QGraphicsView(self.scene,self)
		vue.setFixedSize(802, 405)
		vue.setGeometry(290,10,804,407)
		vue.show()
		# layout_test.addWidget(vue)

		# self.item_list.append(self.scene.addEllipse(10,10,10,10))  # just for test
		self.pen = QtGui.QPen()
		self.brush = QtGui.QBrush(QtGui.QColor("#FF0000"))



	def updateMap(self,id_departure,id_arrival):
		if self.DEBUG == True:
			print "DEBUG: updating map..."

		# we clean the list of points
		if len(self.item_list) > 0:
			for i in range(0, len(self.item_list)):
				self.scene.removeItem(self.item_list[0])
				self.item_list.pop(0)

		# we convert from id to geopositions the whole list
		geo_pos_departure = getGeolocalizationFromID(id_departure)
		geo_pos_arrival   = getGeolocalizationFromID(id_arrival)

		# we convert from geopositions to pixels and we draw the lines
		for i in range(0,len(geo_pos_departure)/2):
			for j in range(0,len(geo_pos_arrival)/2):
				x1,y1 = convertGeopositionToPixel(geo_pos_departure[2*i],geo_pos_departure[2*i+1])
				x2,y2 = convertGeopositionToPixel(geo_pos_arrival[2*j],geo_pos_arrival[2*j+1])
				if abs(x1 - x2) < 400:
					self.item_list.append(self.scene.addLine(x1+4,y1+4,x2+4,y2+4,self.pen))
				else:
					ecart_y = y2 - y1
					if x2 > x1:
						self.item_list.append(self.scene.addLine(x2+4,y2+4,797,y2-((800-x2)/(800-abs(x2-x1)))*ecart_y,self.pen))
						self.item_list.append(self.scene.addLine(x1+4,y1+4,2,y1+(x1/(800-abs(x2-x1)))*ecart_y,self.pen))
					else:
						self.item_list.append(self.scene.addLine(x1+4,y1+4,797,y1+((800-x1)/(800-abs(x1-x2)))*ecart_y,self.pen))
						self.item_list.append(self.scene.addLine(x2+4,y2+4,2,y2-(x2/(800-abs(x1-x2)))*ecart_y,self.pen))


		# we convert from geopositions to pixels and we draw the points
		for i in range(0,len(geo_pos_departure)/2):
			x,y = convertGeopositionToPixel(geo_pos_departure[2*i],geo_pos_departure[2*i+1])
			self.item_list.append(self.scene.addEllipse(x,y,10,10,self.pen,self.brush))
		for i in range(0,len(geo_pos_arrival)/2):
			x,y = convertGeopositionToPixel(geo_pos_arrival[2*i],geo_pos_arrival[2*i+1])
			self.item_list.append(self.scene.addEllipse(x,y,10,10,self.pen,self.brush))


		# self.pen.restore()
		# self.pen.setPen(QtGui.QColor(223, 47, 32))
		# self.pen.setBrush(QtGui.QColor(223, 47, 32))
		# self.pen.drawEllipse(430,100,5,5)
		# self.map_label.setPixmap(self.map_pix)

		if self.DEBUG == True:
			print "DEBUG: map updated"

	def updateList(self, flight_list, flight_list_with_stops):
		if len(flight_list)==0 and len(flight_list_with_stops)==0:
			self.list_model = QtGui.QStandardItemModel(0,1)
			self.list_model.setHorizontalHeaderItem(0, QtGui.QStandardItem("No flights available"))
			self.list_view.setModel(self.list_model)
			return

		self.list_model = QtGui.QStandardItemModel(0,11)  # (ligne,colonnes)
		self.list_model.setHorizontalHeaderItem(0, QtGui.QStandardItem("Stops"))
		self.list_model.setHorizontalHeaderItem(1, QtGui.QStandardItem("Departure city"))
		self.list_model.setHorizontalHeaderItem(2, QtGui.QStandardItem("Departure airport"))
		self.list_model.setHorizontalHeaderItem(3, QtGui.QStandardItem("Code"))
		self.list_model.setHorizontalHeaderItem(4, QtGui.QStandardItem("Take-off"))
		self.list_model.setHorizontalHeaderItem(5, QtGui.QStandardItem("Arrival city"))
		self.list_model.setHorizontalHeaderItem(6, QtGui.QStandardItem("Arrival airport"))
		self.list_model.setHorizontalHeaderItem(7, QtGui.QStandardItem("Code"))
		self.list_model.setHorizontalHeaderItem(8, QtGui.QStandardItem("Arrival T (local)"))
		self.list_model.setHorizontalHeaderItem(9, QtGui.QStandardItem("Duration"))
		self.list_model.setHorizontalHeaderItem(10, QtGui.QStandardItem("Aircraft"))
		self.list_model.setHorizontalHeaderItem(11, QtGui.QStandardItem("Number"))
		
		if len(flight_list)!=0:
			# max_nb_of_lines = 10  # maximum number of lines to display
			num_of_flights = len(flight_list)   # number of flights in the list
			for i in range(0, num_of_flights):  # for each line
				item = QtGui.QStandardItem("no")
				item.setEditable(False)
				self.list_model.setItem(i,0,item)
				for j in range(0,11):  # for each column
					item = QtGui.QStandardItem(flight_list[i][j])
					item.setEditable(False)
					self.list_model.setItem(i,j+1,item)

		if len(flight_list_with_stops)!=0:
			# AFTER DIRECT FLIGHTS, WE ADD THE FLIGHTS WITH STOPS
			num_of_flights_stops = len(flight_list_with_stops)
			print num_of_flights_stops
			for i in range(0, num_of_flights_stops*2):  # for each line
				if i%2 == 1:  # quick'n dirty
					continue

				j = i/2

				item = QtGui.QStandardItem("1/2")
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,0,item)

				# departure IATA
				item = QtGui.QStandardItem(flight_list_with_stops[j][0])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,3,item)
				# arrival IATA
				item = QtGui.QStandardItem(flight_list_with_stops[j][1])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,7,item)
				# departure time
				item = QtGui.QStandardItem(flight_list_with_stops[j][3])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,4,item)
				# arrival time
				item = QtGui.QStandardItem(flight_list_with_stops[j][6])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,8,item)
				# dep city
				item = QtGui.QStandardItem(flight_list_with_stops[j][18])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,1,item)
				# arr city
				item = QtGui.QStandardItem(flight_list_with_stops[j][19])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i,5,item)


				item = QtGui.QStandardItem("2/2")
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,0,item)
				
				# departure IATA
				item = QtGui.QStandardItem(flight_list_with_stops[j][9])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,3,item)
				# arrival IATA
				item = QtGui.QStandardItem(flight_list_with_stops[j][10])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,7,item)
				# departure time
				item = QtGui.QStandardItem(flight_list_with_stops[j][12])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,4,item)
				# arrival time
				item = QtGui.QStandardItem(flight_list_with_stops[j][15])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,8,item)
				# dep city
				item = QtGui.QStandardItem(flight_list_with_stops[j][19])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,1,item)
				# arr city
				item = QtGui.QStandardItem(flight_list_with_stops[j][20])
				item.setEditable(False)
				self.list_model.setItem(num_of_flights+i+1,5,item)




		# self.list_model.appendRow(item)
		self.list_view.setModel(self.list_model)
		self.list_view.setColumnWidth(0,45)  # Stops -> None ou 1/2 2/2
		self.list_view.setColumnWidth(1,95)  # 95 pour le nom de la ville
		self.list_view.setColumnWidth(2,115)  # 115 pour l'aéroport
		self.list_view.setColumnWidth(3,70)
		self.list_view.setColumnWidth(4,60)
		self.list_view.setColumnWidth(5,95)
		self.list_view.setColumnWidth(6,115)
		self.list_view.setColumnWidth(7,70)
		self.list_view.setColumnWidth(8,100)  # LAISSER LA PLACE POUR LE J+1 ou J-1
		self.list_view.setColumnWidth(9,70)
		self.list_view.setColumnWidth(10,160)
		self.list_view.setColumnWidth(11,70)
