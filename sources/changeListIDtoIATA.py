#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def getIATAFromId(id_):
	"""get the IATA from the ID"""
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	query = "SELECT IATA FROM airports WHERE id = '"+str(id_)+"'"
	c.execute(query)

	IataValue = c.fetchone()[0]
	c.close()

	return IataValue
	
def changeListIDtoIATA(list):
	"""transform a ID list into a IATA list and ignore all airports that doesn't have a IATA"""
	for n in range(0,len(list)):
		list[n] = getIATAFromId(list[n])

	while None in list:
		list.remove(None)

	return list

def test():
	list = [57,3,57,5,55,57]
	changeListIDtoIATA(list)
	print list

if  __name__ == '__main__':
	test()
