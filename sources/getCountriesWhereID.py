#!/usr/bin/python2.
# -*-coding:Utf-8 -*   # linux

import sqlite3

def getCountriesWhereID(id):
	"""Return the (distinct) list of countries having an airport which is in the list given as argument"""

	# we create a list to store matching countries
	countries = list()

	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	for n in id:
		query = "SELECT country FROM airports WHERE id='" + str(n) + "' "
		c.execute(query)
		for line in c.fetchall():
			countries.append(line[0])

	c.close()
	return list(set(countries))  # to elimnate items appearing twice

def test():
	id = list()
	id.append(1382)  # Charles de Gaulle
	id.append(1386)  # Orly
	id.append(507)   # Heathrow (London)
	# -> we should get "France" and "United Kingdom"

	countries = getCountriesWhereID(id)
	for n in range(0,len(countries)):
		print countries[n]

if  __name__ == '__main__':
	test()
