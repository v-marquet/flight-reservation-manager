#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

# Comment : this programme check all possible stops from a departure, a arrival and a date handed by the user
# 2nd comment : all possible stops of the day handed, it doesn't take into account the next day

import sqlite3

def instr(a, b):
	"""code of the position function in sql"""
	return a.find(b) + 1

def print_test(rows):				# DEBUG
	i = 0
	for row in rows:
		i += 1
		print (row)
	print (i)

def getSec(time):	# Stackoverflow comment code which convert time format 'HH:MM:SS' to seconds
    l = time.split(':')		#  => l[0] = "HH"   l[1] = "MM"   l[2] = "SS"
    return int(l[0]) * 3600 + int(l[1]) * 60 + int(l[2])

def formatTime(duration):	# INPUT =  duration (in minutes) -> OUTPUT = "HH:MM:SS"
	minutes = int(duration)%60
	hours = int(duration)/60
	if hours < 10:
		hours = "0" + str(hours)
	if minutes <10:
		minutes = "0" + str(minutes)
	return hours + ":" + minutes + ":00" 	

def getAllProbableStopsFromDeparture(departure, date, c):	# 
	# Initialization
	probableStopList = []
	
	# Find probable stops from departure
	query = "SELECT departure, arrival, dep_time, duration FROM flights WHERE (departure = '"+departure+"') AND day_op LIKE '%"+date+"%'"
	c.execute(query)
	probableStopList = list(c)
	#print_test(probableStopList)	# DEBUG

	return probableStopList


def getAllStopsToGoToArrival(departure, arrival, date):	# Final stop list
	# Initialization
	conn = None
	forgetFlag = False	# Flag
	departureReference = 'INIT'	# Initialisation de la variable de ref
	stopList = list()	# List of stops
	hourlyList = []		# Temporary list of a stop departure time
		
	try:
		conn  = sqlite3.connect("../databases/flights.sqlite")
		c = conn.cursor()
		
		# The function name will give you one's job
		probableStopList = getAllProbableStopsFromDeparture(departure, date, c)

		for probableStop in probableStopList:
			# Initialize the hourlyList if we change the place of departure
			#print (probableStop[1]+" and "+departureReference) # DEBUG
			if probableStop[1] != departureReference:
				departureReference = probableStop[1]
				hourlyList = []
				forgetFlag = False
				#print ("Init")		# DEBUG

			# If we have already seen this departure with this hour we move up to the next departure
			for hour in hourlyList:
				if probableStop[2] == hour:
					#print ("check horaire")		# DEBUG
					forgetFlag = True
					probableStopList.remove(probableStop)
					break
				forgetFlag = False

			# We find all
			if forgetFlag == False:	# Delete doubloon
				#print ("ajout horaire sup + requete")	# DEBUG
				hourlyList.append(probableStop[2])	# Add the hour which not present in the list
				#print (hourlyList)	# DEBUG
				tempo = formatTime(str(int(probableStop[3]) + 180))

				# Request
				# Fill in myself !!!
				# ...
				query = """SELECT departure, arrival, dep_time, duration FROM flights WHERE ("""
				query +=   "'" + probableStop[1] + "' = departure) AND (arrival = '" + arrival
				query +=   "') AND day_op LIKE '%"+date+"%' AND ((-'"
				query +=   str(getSec(formatTime(str(int(probableStop[3]) + 180)))+getSec(probableStop[2]+":00"))
				query +=   "' + strftime('%s', dep_time) - strftime('%s', '00:00:00')) >= 0)"
				c.execute(query)
				result = c.fetchall()		# Allocation at result if not we loss datas

				# If the list is empty, the probable stop is delete
				if len(result) == 0:
					probableStopList.remove(probableStop)
				else:
					stopList.append(probableStop)
					stopList.extend(result)
			#print_test(result)		# DEBUG

		#print_test (stopList)		# DEBUG
	except sqlite3.Error, e:
		print ("Error %s:" % e.args[0])
		#sys.exit(1)
	finally:
		if conn:
			conn.close()

	return stopList

def test():
	departure = "CDG"
	arrival = "GVA"
	date = str(7)
	print_test (getAllStopsToGoToArrival(departure,  arrival, date))

if  __name__ == '__main__':
	test()
