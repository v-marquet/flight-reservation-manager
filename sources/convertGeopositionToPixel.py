#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

def convertGeopositionToPixel(lon, lat):
	"""To convert latitude and longitude to a couple of pixels"""
	lon = int(float(lon))
	lat = int(float(lat))

	# the following variables depends on the picture used
	longitude0 = 400  # abscissa of the greenwich meridian in the picture in pixels
	latitude0 = 200   # ordonate of the equator in the picture in pixels
	ratio = 2.2  # number of pixels by degree

	# we suppose that x is oriented from left to right and y from top to bottom
	corner_x = -5  # position on x axis of the left corner of the picture in the program window
	corner_y = -5  # position on y axis of the left corner of the picture in the program window

	# if the longitude is not between -180 and 180 
	if lon < -180 or lon > 180:
		lon = lon % 360
	if lon > 180:
		lon = lon - 360
	# now the longitude is between -180 and 180
	x = corner_x + longitude0 + lon * ratio

	# if the latitude is not between -90 and 90
	if lat < -90 or lat > 90:
		lat = lat % 180
	if lat > 90:
		lat = lat - 180
	# now the latitude is between -90 and 90
	y = corner_y + latitude0 - lat * ratio   # minus because the axis is reversed

	return x,y

def test():
	# Paris coordinates for test
	x,y = convertGeopositionToPixel(2,48)  # FORMAT: longitude,latitude !
	print x
	print y


if  __name__ =='__main__':
	test()
