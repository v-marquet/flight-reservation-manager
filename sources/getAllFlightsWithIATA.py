#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def instr(a, b):
	"""code of the position function in sql"""
	return a.find(b) + 1

def doubleIntToTimeString(hours, minutes):
	if hours < 10:
		hours_string = "0" + str(hours)
	else:
		hours_string = str(hours)
	if minutes <10:
		minutes_string = "0" + str(minutes)
	else:
		minutes_string = str(minutes)
	return hours_string + ":" + minutes_string	

def computeLocalArrivalTime(departure_time, duration, timezone_gap):
	"""Output time format: HH:MM [J+1]"""
	time = departure_time.split(':')
	day = ""   
	hours = int(time[0])
	minutes = int(time[1])	
	# we add duration
	hours += int(duration)/60
	minutes += int(duration)%60
	if minutes > 59:
		minutes = minutes % 60
		hours = hours + 1
	hours = hours + timezone_gap
	if hours > 23:
		hours = hours % 24
		day = "   (J+1)"
	if hours < 0:
		hours = hours % 24
		day = "   (J-1)"
	time_without_day = doubleIntToTimeString(hours, minutes)
	return time_without_day + day

def getAllFlightsWithIATA(departure_list, arrival_list, day):
	"""get all flights information for a departure list, an arrival list and the day of the week"""

	print "DEBUG: entering getAllFlightsWithIATA ..."
	if len(departure_list)==0 or len(arrival_list)==0:
		print "DEBUG: empty list (departure or arrival). No need to make the search in getAllFlightsWithIATA"
		return list()

	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	conn.create_function("instr", 2, instr)
	conn.create_function("computeLocalArrivalTime", 3, computeLocalArrivalTime)
	query = """SELECT DISTINCT DEP.city, DEP.airportName, departure, dep_time, ARR.city, ARR.airportName, arrival, 
				computeLocalArrivalTime(dep_time,duration,a2.timezone - a1.timezone), 
				duration, airlines.airline_name, flightnum,
				a2.timezone - a1.timezone AS dec_horaire
				FROM flights
				INNER JOIN airports AS DEP ON DEP.IATA = flights.departure
				INNER JOIN airports AS ARR ON ARR.IATA = flights.arrival
				INNER JOIN (SELECT IATA, airline_name FROM airlines GROUP BY IATA) as airlines ON airlines.IATA = flights.carrier
				INNER JOIN airports AS a1 ON a1.IATA = flights.departure
				INNER JOIN airports AS a2 ON a2.IATA = flights.arrival
				WHERE """ + convertListToQueryDeparture(departure_list) + " AND " + convertListToQueryArrival(arrival_list)

	query += " AND (instr(day_op, '"+ str(day) +"') != 0) ORDER BY dep_time;"
	c.execute(query)

	liste = list()
	for line in c.fetchall():
		l = list()
		for i in range(0,12):
			l.append(str(line[i]))
		liste.append(l)

	# liste.append(c.fetchone()[0])


	# liste = list(c)
	print liste

	c.close()
	print "DEBUG: leaving getAllFlightsWithIATA"

	return liste

def convertListToQueryDeparture(list):
	# if the list is empty, we return None
	if len(list) == 0:
		return None

	# we convert a list into a part of the query
	query = "("  # we create an empty string
	for n in range(0,len(list)):
		query += "departure='" + str(list[n]) + "' "
		if n != len(list) - 1:
			query += "OR "
	query += ")"

	return query

def convertListToQueryArrival(list):
	# if the list is empty, we return None
	if len(list) == 0:
		return None

	# we convert a list into a part of the query
	query = "("  # we create an empty string
	for n in range(0,len(list)):
		query += "arrival='" + str(list[n]) + "' "
		if n != len(list) - 1:
			query += "OR "
	query += ")"

	return query

def test():
	depList = ['CDG']
	arrList = ['FRA','ACC','LHR']

	print "\nConvert list to query:"
	print convertListToQuery(depList)
	print convertListToQuery(arrList)
	
	print getAllFlightsWithIATA(depList, arrList, 1)

	

if  __name__ == '__main__':
	test()

