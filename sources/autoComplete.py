#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def autoComplete(word):
	"""Search for autocompletion possibilities"""
	word = word.upper()
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	query = """SELECT DISTINCT A FROM (SELECT airports.city as A FROM airports WHERE UPPER(airports.city) LIKE """ + "'" + str(word) + "%'"
	query += """ UNION ALL SELECT airports.airportName as A FROM airports WHERE UPPER(airports.airportName) LIKE """ + "'" + str(word) + "%'" + ");"
	c.execute(query)

	l = list()
	for line in c.fetchall():
		# print line[0]
		l.append(line[0])

	c.close()
	return l
	
def test():
	w = "Lon"
	print autoComplete(w)


if  __name__ == '__main__':
	test()
