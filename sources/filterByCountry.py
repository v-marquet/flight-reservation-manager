#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def filterByCountry(ID_list, country):
	"""return a list with only the id of airports in the given country"""
	
	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	query = "SELECT id FROM airports WHERE (country = '"+country+"')"
	c.execute(query)

	id_of_airports_in_country = list()
	for line in c.fetchall():
		id_of_airports_in_country.append(line[0])

	l = list(set(ID_list).intersection(id_of_airports_in_country))

	c.close()

	return l

def test():
	ID_list = list()
	print ID_list
	ID_list = filterByCountry(ID_list, "Iceland")
	print ID_list  # should return "15"

if  __name__ == '__main__':
	test()
