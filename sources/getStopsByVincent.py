#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

# TEST: en console, depuis la racine du projet:
#  python sources/getStopsByVincent.py   

import sqlite3
import sys

def getMinFromTime(time):
	"""convert time format "HH:MM" to minutes"""
	l = time.split(':')   #  => l[0] = "HH"   l[1] = "MM"
	return int(l[0]) * 60 + int(l[1])

def formatTimeFromMin(duration):
	"""INPUT =  duration (in minutes) -> OUTPUT = "HH:MM" """
	minutes = int(duration)%60
	hours = int(duration)/60
	return doubleIntToTimeString(hours, minutes)

def doubleIntToTimeString(hours, minutes):
	if hours < 10:
		hours_string = "0" + str(hours)
	else:
		hours_string = str(hours)
	if minutes <10:
		minutes_string = "0" + str(minutes)
	else:
		minutes_string = str(minutes)
	return hours_string + ":" + minutes_string	


def isItNextDayTime(time):
	"""INPUT: HH:MM. Return 1 if time > 23:59, else return 0"""
	l = time.split(':')	
	if int(l[0]) > 23:
		return 1
	else:
		return 0 

def isItNextDayDuration(duration):
	"""INPUT: duration in minutes. Return 1 if duration > 24*60, else return 0"""
	if duration >= 24*60:
		return 1
	else:
		return 0 

def nextWeekDay(day):
	"""To get the number of the next day"""
	if day < 7:
		day = day + 1
	else:  # day = 7
		day = 1
	return day

def computeLocalArrivalTime(departure_time, duration, timezone_gap):
	"""Output time format: DD:HH:MM"""
	time = departure_time.split(':')
	day = 1   # relative reference, not absolute
	hours = int(time[0])
	minutes = int(time[1])	
	# we add duration
	hours += int(duration)/60
	minutes += int(duration)%60
	if minutes > 59:
		minutes = minutes % 60
		hours = hours + 1
	hours = hours + timezone_gap
	if hours > 23:
		hours = hours % 24
		day = day + 1
	if hours < 0:
		hours = hours % 24
		day = day - 1
	time_without_day = doubleIntToTimeString(hours, minutes)
	return "0" + str(day) + ':' + time_without_day





def getStopsByVincent(day):
	"""To get stops (une escale uniquement)"""

	margin_min_between_flights = 90    # minutes
	margin_max_between_flights = 300   # minutes

	next_day = nextWeekDay(day)

	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()

	# we create SQL functions from Python functions, to deal with time formats
	conn.create_function("getMinFromTime", 1, getMinFromTime)
	conn.create_function("formatTimeFromMin", 1, formatTimeFromMin)
	conn.create_function("computeLocalArrivalTime", 3, computeLocalArrivalTime)

	# VERSION 1: gestion du jour mais pas de l'heure
	query = """SELECT f1.departure, f1.arrival, f1.day_op, f1.dep_time, f1.duration, a2.timezone - a1.timezone AS dec_horaire, 
		computeLocalArrivalTime(f1.dep_time,f1.duration,a2.timezone - a1.timezone),
		f2.departure, f2.arrival, f2.day_op, f2.dep_time, f2.duration, a4.timezone - a2.timezone AS dec_horaire,
		computeLocalArrivalTime(f2.dep_time,f2.duration,a4.timezone - a2.timezone)
	FROM flights AS f1 
	INNER JOIN flights AS f2 ON f1.arrival=f2.departure 
	INNER JOIN airports AS a1 ON a1.IATA = f1.departure
	INNER JOIN airports AS a2 ON a2.IATA = f1.arrival
	INNER JOIN airports AS a4 ON a4.IATA = f2.arrival
	WHERE f1.departure='CDG' AND f2.arrival='JFK'
		AND f1.day_op LIKE '%""" + str(day) + """%' 
		AND ( f2.day_op LIKE '%""" + str(day) + """%' OR f2.day_op LIKE '%""" + str(next_day) + """%' ); """

	# VERSION 2: ajout de la gestion (partielle) de l'heure
	# query = """SELECT f1.departure, f1.arrival, f1.day_op, f1.dep_time, f1.duration, 
	# 	f2.departure, f2.arrival, f2.day_op, f2.dep_time, f2.duration
	# FROM flights AS f1 
	# INNER JOIN flights AS f2 ON f1.arrival=f2.departure 
	# WHERE f1.departure='CDG' AND f2.arrival='AGA'
	# 	AND f1.day_op LIKE '%""" + str(day) + """%' 
	# 	AND ( f2.day_op LIKE '%""" + str(day) + """%' OR f2.day_op LIKE '%""" + str(next_day) + """%' )
	# 	AND getMinFromTime(f2.dep_time) BETWEEN ( getMinFromTime(f1.dep_time) + f1.duration + 90) 
	# 									AND ( getMinFromTime(f1.dep_time) + f1.duration + 300) ; """
	# WARNING: ça filtre bien selon l'heure pour le jour N,
	# mais cette heure est aussi utilisée pour filtrer le jour N+1, 
	# donc ça supprime des résultats potentiellements intérrêssants 

	# SOLUTION DE FACILITÉ: SE CONTENTER DE LA REQUETE CI DESSUS (V2)
	# ET FAIRE L'ÉCRÉMAGE NÉCÉSSAIRE EN PYTHON

	# VERSION 3: amélioration de la gestion de l'heure:
	# pour le jour N, rien ne change
	# pour le jour N+1, on ne prend que les vols entre minuit et 5h (car proche du jour N)
	# query = """SELECT f1.departure, f1.arrival, f1.day_op, f1.dep_time, f1.duration, 
	# 	f2.departure, f2.arrival, f2.day_op, f2.dep_time, f2.duration
	# FROM flights AS f1 
	# INNER JOIN flights AS f2 ON f1.arrival=f2.departure 
	# WHERE f1.departure='CDG' AND f2.arrival='AGA'
	#	AND f1.day_op LIKE '%""" + str(day) + """%' 
	#	AND ( 	( f2.day_op LIKE '%""" + str(day) + """%' AND getMinFromTime(f2.dep_time) BETWEEN ( getMinFromTime(f1.dep_time) + f1.duration + 90) 
	#			AND ( getMinFromTime(f1.dep_time) + f1.duration + 300) )
	#		OR	( f2.day_op LIKE '%""" + str(next_day) + """%' AND getMinFromTime(f2.dep_time) < 300	)	)
	#;"""


	c.execute(query)

	liste = list()
	for line in c.fetchall():
		print ""   # to jump a line
		l = list()
		for i in range(0,14):
			if i==7:
				print ""    # to jump a line
			# 	sys.stdout.write("      ")
			l.append(str(line[i]))
			sys.stdout.write(str(line[i]))
			sys.stdout.write("   ")
		liste.append(l)
		# min = getMinFromTime(line[3]) + int(line[4]) + 90
		# max = getMinFromTime(line[3]) + int(line[4]) + 300
		# print " --> " + formatTimeFromMin(min) + "  " + formatTimeFromMin(max)
		print ""   # to jump a line


	# liste.append(c.fetchone()[0])


	# liste = list(c)
	# print liste

	c.close()



def test():
	day = 7
	print getStopsByVincent(day)
	# print computeLocalArrivalTime("00:01", 140, -7)

if  __name__ == '__main__':
	test()