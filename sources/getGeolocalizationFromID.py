#!/usr/bin/python2.7
# -*-coding:Utf-8 -*   # linux

import sqlite3

def getGeolocalizationFromID(id):
	"""Return a list of geopositions (lon,lat) from a list of id of airports (to draw points on the map)"""

	# we create a list to store matching countries
	geo_pos = list()

	conn  = sqlite3.connect("databases/flights.sqlite")
	c     = conn.cursor()
	for n in id:
		query = "SELECT longitude,latitude FROM airports WHERE id='" + str(n) + "' "
		c.execute(query)
		for line in c.fetchall():
			geo_pos.append(line[0])
			geo_pos.append(line[1])

	c.close()
	return geo_pos

def test():
	id = list()
	id.append(1382)  # Charles de Gaulle
	id.append(1386)  # Orly
	id.append(507)   # Heathrow (London)
	# -> we should get "France" and "United Kingdom"

	geo_pos = getGeolocalizationFromID(id)
	for n in range(0,len(geo_pos)):
		print geo_pos[n]

if  __name__ == '__main__':
	test()
